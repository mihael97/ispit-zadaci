class Node:
    """
    Predstavlja svako stanje u kojem se pretraživanje može nalaziti
    """

    def __init__(self, values: tuple, limits: tuple, target: int, visited: set):
        """
        Konstruktor
        :param values:  trenutne vrijednosti količine u menzurama
        :param limits: zapremnina menzura
        :param target: ciljana vrijednost u menzuri
        :param visited: posjećene vrijednosti
        """
        self.values = values
        self.limits = limits
        self.target = target
        self.visited = visited


def calculate(node: Node):
    """
    Rekurzivno rješavanje problema. Za pretraživanje se koristi BFS(https://en.wikipedia.org/wiki/Breadth-first_search),
    a uz manje izmjene moguće i pomoću DFS(https://en.wikipedia.org/wiki/Depth-first_search)
    :param node: trenutno stanje
    :return: -1 ako rješenje nije pronađeno, inače najmanji broj koraka
    """
    result = -1

    search_values = set()  # set koji predstavlja koja će se nova stanja posjetiti na sljedećoj razini
    for i in range(len(node.values)):  # izvor tekućine
        for j in range(len(node.values)):  # odredište tekućine
            if i == j:
                # ne možemo prelijevati u istu menzuru
                continue
            # izvor će postati prazan (preliti ćemo sve u odredište) ili se uzme samo dio količine
            first = max(0, node.values[i] - (node.limits[j] - node.values[j]))

            # u odredište se nadolijeva sva količina iz izvora ili se nadolijeva do limita
            second = min(node.values[i] + node.values[j], node.values[j] + (node.limits[j] - node.values[j]))
            values_copy = list(node.values)  # node.values je tuple koji se ne može mjenjati, potrebno pretvoriti listu
            values_copy[i] = first  # postavljanje nove količine izvora
            values_copy[j] = second  # postavljanje nove količine odredišta
            values_copy = tuple(
                values_copy)  # pretvarnje u tuple (potrebno jer se lista može mjenjati pa se zbog toga ne može dodati u set)

            if node.target in values_copy:  # provjera da li je postignuta tražena količina u bilo kojoj menzuri
                # (kraj pretraživanja)
                return 1  # potreban jedan(ovaj) korak

            if not (
                    values_copy in node.visited):  # provjera da li je stanje posjećeno prije ovog koraka pretraživanja
                search_values.add(
                    values_copy)  # dodavanje stanja za sljedeće pretraživanje, kako je search_values set duplikati nisu omogućeni
    for values_copy in search_values:  # pretraživanje ove dubine, sva moguća stanja
        node.visited.add(values_copy)  # dodavanje u posjećena stanja
        pom_result = calculate(Node(values_copy, node.limits, node.target, node.visited))  # rekurzivni spust
        if pom_result != -1 and (
                result == -1 or pom_result < result):  # ako je pom_result -1, rezultat nije pronađen, ako je result=-1
                                                        # postavi na jer je to prvi rezultata inače traži manjeg
            result = pom_result + 1  # potreban jedan dodatan korak
    return result


def main():
    with open('input/epruvete2.txt', 'r') as file:
        lines = file.readlines()
        target = int(lines[0])
        volumes = [int(x) for x in lines[1].strip().split(' ')]
        current_volumes = [int(x) for x in lines[2].strip().split(' ')]

    print(calculate(
        Node((current_volumes[0], current_volumes[1], current_volumes[2]), (volumes[0], volumes[1], volumes[2]), target,
             set())))


if __name__ == '__main__':
    main()
