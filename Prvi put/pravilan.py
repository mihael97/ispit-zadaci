def main():
    with open("input/pravilan.txt") as file:
        lines = file.readlines()
        root_word = lines[0]
        number_of_words = int(lines[1])
        words = [lines[i].strip() for i in range(2, 2 + number_of_words)]

    def check_word(word: str):
        word_index = 0  # index slova u riječi koju provjeravamo
        offset = -1  # traženi odmak između indexa
        last_index = -1  # u glavnoj riječi, index kada se pojavilo zadnje slovo riječi koju provjeravamo
        for i, x in enumerate(root_word):  # prolazak kroz glavnu riječ
            if x != word[word_index]:  # ako se slova ne podudaraju, nastavi
                continue
            if last_index != -1:  # zajedničko slovo koje nije prvo
                if offset == -1:  # drugo zajedničko slovo, postavi traženi odmah između slova
                    offset = i - last_index
                elif offset != i - last_index:  # 3 ili više zajedničko slovo, provjeri da li je odmak isti
                    break
                if word_index == len(word) - 1:  # provjeri da li smo došli do kraja riječi koju provjeravamo
                    break
            last_index = i  # index slova u glavnoj riječi
            word_index += 1  # slovo pronađeno, micanje u riječi koju pretražujemo
        return "DA" if word_index == len(
            word) - 1 else "NE"  # DA ako smo došli do kraja pretraživanje u riječi, inače NE

    for word in words:
        print(check_word(word))


if __name__ == '__main__':
    main()
