def main():
    cave = []

    def get_difference(index: int):
        """
        Pronalazi razliku (broj točaka) u stupcu
        :param index: index stupca
        :return: udaljenost stropa i poda
        """
        count = 0
        for x in cave:
            if x[index] == '.':
                count += 1
        return count

    with open('Prvi put/input/spilja2.txt', 'r') as file:
        lines = file.readlines()
        dimensions = [x for x in lines[0].strip()]
        dimensions = (int(dimensions[0]), int(dimensions[2]))
        for line in lines[1:]:
            cave.append([x for x in line.strip()])
    minimum = dimensions[0] + 1  # postavimo kao max na visinu špilje + 1(nemoguće, prvi je uvijek manji)
    for i in range(dimensions[1]):
        minimum = min(minimum, get_difference(i))  # pretražujemo za svaki stupac i nalazimo minimum

    for i in range(dimensions[0]):  # ispis
        if i < minimum:  # prvih minimum redaka špilja 'propada', nalaze se točkice
            print("".join(['.' for _ in range(dimensions[1])]))
        else:  # za ostale redove, ako je prvotno bila stijena ona i ostaje, inače provjera da li je gornja stijena
            # popunila rupu(red za minimum manji bila stijena)
            print("".join([(cave[i][x] if cave[i][x] == '#' else cave[i - minimum][x]) for x in range(dimensions[1])]))


if __name__ == '__main__':
    main()
