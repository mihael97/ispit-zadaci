def main():
    dimensions = []
    [dimensions.append(tuple(map(int, x.split()))) for x in open("input/parcela.txt").readlines()]
    min_result = float('inf')

    for a in range(3):
        for b in range(3):
            if a == b:
                continue
            for c in range(3):
                if c in (a, b):
                    continue
                for rotation in range(8):
                    copy = dimensions.copy()

                    for i in range(3):
                        if (rotation >> i) & 1:
                            copy[i] = (copy[i][1], copy[i][0])

                    def calculate_biggest_area(pos_b, pos_c):
                        x = max(copy[a][0], pos_b[0] + copy[b][0], pos_c[0] + copy[c][0])
                        y = max(copy[a][1], pos_b[1] + copy[b][1], pos_c[1] + copy[c][1])
                        return x * y

                    pos_b = (copy[a][0], 0)
                    for pos_c in [(0, copy[a][1]), (copy[a][0] + copy[b][0], 0), (copy[a][0], copy[b][1])]:
                        if pos_c == (0, copy[a][1]) and copy[c][0] > copy[a][0] and copy[b][1] > copy[a][1]:
                            continue
                        min_result = min(min_result, calculate_biggest_area(pos_b, pos_c))

                    pos_b = (0, copy[a][1])
                    for pos_c in [(copy[a][0], 0), (0, copy[a][1] + copy[b][1]), (copy[b][0], copy[a][1])]:
                        if pos_c == (copy[a][0], 0) and copy[b][0] > copy[a][0] and copy[c][1] > copy[a][1]:
                            continue
                        min_result = min(min_result, calculate_biggest_area(pos_b, pos_c))
    print(min_result)


if __name__ == '__main__':
    main()
