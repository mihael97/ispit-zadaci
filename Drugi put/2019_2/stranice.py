def main():
    with open("input/stranice3.txt", 'r') as f:
        for index, line in enumerate(f.readlines()):
            if index == 0:
                size = int(line)
            else:
                edges = line
    items = []
    item = 1
    for i in range(size):
        row = []
        for y in range(i + 1):
            row.append(item)
            item += 1
        items.append(row)
    for edge in edges:
        if edge == "A":
            items = [x[1:] for x in items]
        elif edge == "B":
            items = [x[:-1] for x in items]
        else:
            items = items[:-1]
        items = [ele for ele in items if ele != []]
        sum = 0
        for line in items:
            for x in line:
                sum += x
        if len(items) != 0:
            print(sum)


if __name__ == '__main__':
    main()
