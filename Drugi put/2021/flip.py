def main():
    size = -1
    numbers = []
    with open("input/flip2.txt", 'r') as file:
        for index, line in enumerate(file.readlines()):
            if index == 0:
                size = int(line)
            else:
                numbers = [int(x) for x in line.strip()]

    dp = [[[0 for _ in range(2)] for _ in range(2)] for _ in range(size + 1)]
    dp[0] = [[i for i in range(2)] for _ in range(2)]

    for index in range(size):  # index u nizu
        for i in range(2):  # vrijednost (0 ili 1)
            for j in range(2):  # broj flipova
                current_item = numbers[index] if j % 2 == 0 else 1 - numbers[index]
                dp[index + 1][i][j] = dp[index][i][j] if current_item == i else 1 + min(
                    dp[index][i][j], dp[index][1 - i][j], dp[index][i][1 - j])
    print(min(dp[-1][0]))  # trebamo 0 na kraju


if __name__ == '__main__':
    main()
