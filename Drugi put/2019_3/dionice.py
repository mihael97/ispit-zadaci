def main():
    with open("input/dionice.txt", 'r') as file:
        lines = file.readlines()
        n = int(lines[0])
        stocks = [tuple([int(x) for x in lines[i].split()]) for i in range(1, len(lines))]
    max_value = 0
    bought = set()
    sold = set()
    for type, price in stocks:
        if type == 2:
            bought.add(price)
        else:
            total = float('-Inf')
            if len(bought) != 0:
                total = price - list(bought)[0]
            subs = float('-Inf')
            if len(sold) != 0:
                subs = price - list(sold)[0]
            if total > subs and total > 0:
                sold.add(price)
                bought = set(list(bought)[1:])
                max_value += total
            elif subs >= total and subs > 0:
                sold = set(list(sold)[1:])
                sold.add(price)
                max_value += subs
    print(max_value)


if __name__ == '__main__':
    main()
