def main():
    land = []
    size = -1
    with open("input/kvadrat3.txt") as file:
        lines = file.readlines()
        for index, line in enumerate(lines):
            if index == 0:
                size = int(line)
            else:
                land.append([int(x) for x in line.strip().split(" ")])
    max_value = -1000000000
    for i in range(size):
        for x in range(size):  # pocetak
            for y in range(size):  # kraj
                if y + i >= size or x + i >= size:
                    continue
                sum = 0
                for x_pos in range(x, x + i + 1):
                    for y_pos in range(y, y + i + 1):
                        coeff = min(x_pos, y_pos, size - 1 - x_pos, size - 1 - y_pos) + 1
                        sum += coeff * land[x_pos][y_pos]
                max_value = max(sum, max_value)
    print(max_value)


if __name__ == '__main__':
    main()
