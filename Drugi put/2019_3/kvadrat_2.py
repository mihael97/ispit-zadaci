def main():
    with open('input/kvadrat.txt') as f:
        lines = f.readlines()
        size = int(lines[0])
        pref = [[0] * (size + 1) for _ in range(size + 1)]
        for i in range(1, len(lines)):
            items = [float(x) for x in lines[i].split(" ")]
            for index, element in enumerate([pref[i - 1][j] + sum(items[:j]) for j in range(1, size + 1)]):
                pref[i][index + 1] = element

    def get_sum(r1, r2, s1, s2):
        return pref[r2][s2] - pref[r1 - 1][s2] - pref[r2][s1 - 1] + pref[r1 - 1][s1 - 1]

    max_value = float('-INF')
    for i in range(2):
        for row in range(1, size + 1):
            for column in range(1, size + 1):
                r1, r2, s1, s2 = row, row + i, column, column + i
                if r2 > size or s2 > size:
                    continue
                max_sum = get_sum(r1, r2, s1, s2)
                max_value = max(max_value, max_sum)
                while r1 > 1 and r2 < size and s1 > 1 and s2 < size:
                    r1, r2, s1, s1 = r1 - 1, r2 + 1, s1 - 1, s2 + 1
                    max_sum += get_sum(r1, r2, s1, s2)
                    max_value = max(max_value, max_sum)
    print(max_value)


if __name__ == '__main__':
    main()
